package com.fourcatsdev.entitycrudrest.controle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fourcatsdev.entitycrudrest.dto.EstudanteCreateDTO;
import com.fourcatsdev.entitycrudrest.dto.EstudanteResponseDTO;
import com.fourcatsdev.entitycrudrest.dto.mapper.EstudanteMapper;
import com.fourcatsdev.entitycrudrest.excecao.EstudanteNotFoundException;
import com.fourcatsdev.entitycrudrest.modelo.Estudante;
import com.fourcatsdev.entitycrudrest.servico.EstudanteServico;

@RestController
@RequestMapping("/estudantes")
public class EstudanteControle {
	
	@Autowired
	private EstudanteServico estudanteServico;
	
	@Autowired
	private EstudanteMapper estudanteMapper;
	
	@PostMapping
    public ResponseEntity<EstudanteResponseDTO> salvar(@RequestBody EstudanteCreateDTO estudanteCreateDTO) {
        Estudante estudante = estudanteMapper.toEntity(estudanteCreateDTO);
        Estudante estudanteGravado = estudanteServico.gravar(estudante);
        EstudanteResponseDTO estudanteResponseDTO = estudanteMapper.toDTO(estudanteGravado);        
        return ResponseEntity.status(HttpStatus.CREATED).body(estudanteResponseDTO);
    }
	
	@GetMapping
    public ResponseEntity<List<EstudanteResponseDTO>> buscarTodos() {
        List<Estudante> estudantes = estudanteServico.buscarTodos();
        List<EstudanteResponseDTO> estudantesResponseDTO = estudanteMapper.toDTO(estudantes);        
        return ResponseEntity.status(HttpStatus.OK).body(estudantesResponseDTO);
    }
	
	@GetMapping("/{id}")
    public ResponseEntity<Object> buscarUm(@PathVariable(value = "id") Long id) {		
		try {
			Estudante estudanteGravado  = estudanteServico.buscarEstudantePorId(id);
			EstudanteResponseDTO estudanteResponseDTO = estudanteMapper.toDTO(estudanteGravado);        
	        return ResponseEntity.status(HttpStatus.OK).body(estudanteResponseDTO);
		} catch (EstudanteNotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
    }

}
